
// -----------------------------------------------
// Sumador completo de un bit
// -----------------------------------------------
// Entradas: A, B, Cin
// Salida Cout y S


module sumador_completo (
  input a, 
  input b, 
  input ci, 
  output s, 
  output co);

  parameter
    PwrC = 0;
  wire x1;
  xor3_p #(PwrC) xor1 (
    .b(a),
    .c(b),
    .d(ci),
    .a(s)
  );

  


  and2_p #(PwrC) and0 (
    .b(ci),
    .c(a),
    .a(x2)
  );

  and2_p #(PwrC) and1 (
    .b(ci),
    .c(b),
    .a(x3)
  );

  and2_p #(PwrC) and2 (
    .b(a),
    .c(b),
    .a(x4)
  );

  or3_p #(PwrC) or0(
    .b(x2),
    .c(x3),
    .d(x4),
    .a(co)
  );

endmodule


// -----------------------------------------------
// Sumador rizado de 8 bits
// -----------------------------------------------
module SUM_RIZADO#(
    parameter PwrC = 0,
    parameter BITS = 8
    
)(
  input [BITS-1:0] a, 
  input [BITS-1:0] b, 
  input ci, 
  output [BITS-1:0] s, 
  output co);

  assign C[0] = ci;
  assign co = C[BITS];

    

  wire [BITS:0] C;
  genvar i;
  generate
    for(i=0;i<BITS;i=i+1)begin:sum
        
        sumador_completo #(PwrC) sumador( .a(a[i]),.b(b[i]), .ci(C[i]), .s(s[i]), .co(C[i+1]) );
    end
  endgenerate

  


endmodule
