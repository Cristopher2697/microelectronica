### Descripción del proyecto

- Se implementan modulos sumadores de rizado de 1b
- Se implementa sumador de rizado de 8b a partir de modulos de 1b
- Se realizan pruebas donde se mide el consumo de potencia


------------

### Makefile

- El makefile posee tres objetivos :

  1. all: ejecuta el programa y abre automaticamente gtk
  2. vvp: solo ejecuta el programa 
  3. gtk: solo muestra las ondas en gtkwave


- Para ejecutar las pruebas basta con abrir una terminal dentro del directorio src y ejecutar:
`make` 
- Tambien puede probar
`make all`
`make vvp` 
`make gtk` 


------------
### Dependencias

- Debe tener icarus verilog 
`sudo apt-get install iverilog`
- Debe tener gtkwave
`sudo apt-get install gtkwave`









