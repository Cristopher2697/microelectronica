### Descripción del proyecto

- EL MAKEFILE SE ENCUENTRA EN EL DIRECTORIO src/
-  Se realiza la verificación funcional de tres módulos contadores para garantizar el correcto funcionamiento de los diseños.1. 
- Se realiza una descripción detallada del proyecto en el documento llamado Reporte ubicado en el directorio Reporte.
- Se implementa una biblioteca de compuertas
- Se sintetiza modulo contador y verifica bajo las mismas pruebas


------------

### Makefile

- El makefile posee tres objetivos :

  1. contadores: Ejecuta todas las pruebas implementadas y muestra las gráficas en gtkwave usando el archivo previamente creado de gtkw.
  2. timing: Hace lo mimso que el primer objetivo pero toma en cuenta los tiempos de propagación, setup y hold en la simulación
  3. clean: Elimina la carpeta tmp que contiene todos los archivos creados por la simulacion: log, .vcd, .obj


- Para ejecutar las pruebas basta con abrir una terminal dentro del directorio src y ejecutar:
`make` 
- Tambien puede probar
`make contadores`
`make timing` ----->Simulación con tiempos de propagacion
`make clean` 
- El makefile se encuentra en el directorio src/

------------
### Dependencias

- Debe tener icarus verilog 
`sudo apt-get install iverilog`
- Debe tener gtkwave
`sudo apt-get install gtkwave`
- Debe tener Yosys
`sudo apt-get install yosys`








