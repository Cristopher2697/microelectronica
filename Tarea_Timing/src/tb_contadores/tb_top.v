
`timescale 1ns / 1ps

`include "clock.v"
`include "contador.v"
`include "count_tb.v"
`include  "contador_synt.v"
`include  "Estructural/lib/cmos_cells.v"

module tb_top;



    wire [3:0] D, Q_A,Q_SYNT;
    wire [1:0] MODO;

    clk clock (.clock (CLK));

    count_tb  count_tb(
    .CLK (CLK),
    .ENABLE (ENABLE),
    .RESET (RESET),
    .D (D),
    .MODO (MODO),
    .Q_A (Q_A),
    .RCO_A (RCO_A),
    .LOAD_A (LOAD_A),
    .Q_SYNT (Q_SYNT),
    .RCO_SYNT (RCO_SYNT),
    .LOAD_SYNT (LOAD_SYNT)
    );

    contador_cond contador_A(
    .CLK (CLK),
    .ENABLE (ENABLE),
    .RESET (RESET),
    .D (D),
    .MODO (MODO),
    .Q (Q_A),
    .RCO (RCO_A),
    .LOAD (LOAD_A)
    );

    contador_synt contador_synt(
    .CLK (CLK),
    .ENABLE (ENABLE),
    .RESET (RESET),
    .D (D),
    .MODO (MODO),
    .Q (Q_SYNT),
    .RCO (RCO_SYNT),
    .LOAD (LOAD_SYNT)
    );
    
    DFF DFF(
    .C(CLK),
    .D(D[0]),
    .Q(OUT)    
    );
endmodule




