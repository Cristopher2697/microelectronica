task monitor;
    input integer iteration;

    reg mon_LOAD_A, mon_RCO_A,mon_LOAD_SYNT, mon_RCO_SYNT;
    reg [3:0] mon_Q_SYNT,mon_Q_A;
    repeat (iteration) @ (posedge CLK) begin
        mon_Q_A <= Q_A;
        mon_LOAD_A<= LOAD_A;
        mon_RCO_A <= RCO_A;

        mon_Q_SYNT <= Q_SYNT;
        mon_LOAD_SYNT<= LOAD_SYNT;
        mon_RCO_SYNT <= RCO_SYNT;
    end
 
endtask
