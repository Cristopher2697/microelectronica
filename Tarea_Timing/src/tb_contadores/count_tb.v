`timescale 1ns / 1ps

// Testbench Code Goes here
`include "scoreboard_cont.v"


module count_tb #(
    parameter BITS = 4
)(
    input CLK,
    output reg ENABLE,
    output reg RESET,
    output reg [BITS-1:0] D,
    output reg [1:0] MODO,
    input [BITS-1:0] Q_A,
    input  RCO_A,
    input  LOAD_A,
    input [BITS-1:0] Q_SYNT,
    input  RCO_SYNT,
    input  LOAD_SYNT

);

`include "checker_cont.v"
`include "driver.v"
`include "monitor.v"

parameter ITERATIONS = 800;
integer log;

reg [BITS-1:0] Q_s;

initial begin

  $dumpfile("tmp/test.vcd");
  $dumpvars(0);
  
  log = $fopen("tmp/tb.log");
 

  $fdisplay(log, "time=%5d, Simulation Start", $time); ////////
  $fdisplay(log, "time=%5d, Starting Reset", $time); ///////


  $fdisplay(log, "time=%5d, Reset Completed", $time); /////

  $fdisplay(log, "time=%5d, Starting Test", $time);
  fork
    drv_init();
    drv_prueba(ITERATIONS);  
    checker(ITERATIONS);
    monitor(ITERATIONS);
  join


  $fdisplay(log, "time=%5d, Test Completed", $time);
  $fdisplay(log, "time=%5d, Simulation Completed", $time);
  $fclose(log);
  #200 $finish;
end


scoreboard_cont scoreboard_cont(
    .CLK (CLK),
    .ENABLE (ENABLE),
    .RESET (RESET),
    .D (D),
    .MODO (MODO),
    .Q (Q_s),
    .RCO (RCO_s),
    .LOAD (LOAD_s)
    );


endmodule
