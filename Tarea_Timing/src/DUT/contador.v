
module contador_cond (
    input CLK,
    input ENABLE,
    input RESET,
    input [3:0] D,
    input [1:0] MODO,
    output reg [3:0] Q,
    output reg  RCO,
    output reg  LOAD

);

//reg [1:0] NEXT_MODO;

// Sincronización de las señales .
always @(posedge CLK) begin
    if(RESET)
        begin
            Q<=0;
            RCO<=1'b0;
            LOAD<=0;
        end
    else
    begin
        if(ENABLE)
        begin
            
            case (MODO)
                2'b00:
                    begin
                        {RCO,Q}<=Q+3;
                        LOAD<=0;
                    end

                2'b01:
                    begin
                        {RCO,Q}<=Q-1;
                        LOAD<=0;
                    end

                2'b10:
                    begin
                        {RCO,Q}<=Q+1;
                        LOAD<=0;
                    end

                2'b11:
                    begin
                        Q<=D;
                        RCO<=1'b0;
                        LOAD<=1;
                    end
            endcase
        end
        else
        begin
            Q<='bz;
        end
    end
    
end










endmodule // checker

