
`timescale 1ns / 1ps

//Ejemplo #1
module BUF(A, Y);
  specify
	  specparam tpd= 4.5; //hoja de fabricante 74ACT244
    (A*> Y) = (tpd, tpd); //tRise,tFall
  endspecify

  input A;
  output Y;
  assign Y = A;
endmodule

//Ejemplo #2
module BUFX2 (A, Y);
  input  A ;
  output Y ;
  buf (Y, A);
  specify
    // delay parameters
    specparam
      tpllh = 0.13,
      tphhl = 0.15;
    // path delays
    (A *> Y) = (tpllh, tphhl);
  endspecify
endmodule


// Entrada A
// Salida Y
module NOT(A, Y);
  input A;
  output Y;
  assign Y=~A;
  specify
    specparam
      tpdh = 1.6,
      tpdl = 1.6;
    ( A *> Y ) = (tpdh, tpdl);
  endspecify

endmodule

// Entrada A, B
// Salida Y
module NAND(A, B, Y);
  input A, B;
  output Y;
  assign Y = ~(A & B);
  specify
    specparam
      tpdh = 9,
      tpdl = 9;
      ( A, B *> Y ) = (tpdh, tpdl);
  endspecify
endmodule
// Entrada A, B
// Salida Y
module NAND3(A, B, C, Y);
  input A, B, C;
  output Y;
  assign Y = ~(A & B & C);
  specify
    specparam
      tpdh = 9,
      tpdl = 10;
      ( A, B, C *> Y ) = (tpdh, tpdl);
  endspecify
endmodule

// Entrada A, B
// Salida Y
module NOR(A, B, Y);
  input A, B;
  output Y;
  assign Y = ~(A | B);
  specify
    specparam
      tpdh = 9,
      tpdl = 9;
      ( A, B *> Y ) = (tpdh, tpdl);

  endspecify

endmodule

// Entrada A, B
// Salida Y
module NOR3(A, B, C, Y);
  input A, B, C;
  output Y;
  assign Y = ~(A | B | C);
  specify
    specparam
      tpdh = 10,
      tpdl = 10;
      ( A, B, C *> Y ) = (tpdh, tpdl);
  endspecify

endmodule

// Entrada D
// Reloj C
// Salida Q
module DFF(C, D, Q);
  input C, D;
  output reg Q;
  always @(posedge C)
    Q <= D;
  specify
    specparam
      tpdh = 6,
      tpdl = 6,
      tsetup = 20,
      thold = 5;
      ( C, D *> Q ) = (tpdh, tpdl);
      $setup(D, posedge C, tsetup);
      $hold(posedge C, D, thold);
  endspecify
endmodule
