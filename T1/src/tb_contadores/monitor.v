task monitor;
    input integer iteration;

    reg mon_LOAD_A, mon_RCO_A,mon_LOAD_B, mon_RCO_B,mon_LOAD_C, mon_RCO_C;
    reg [3:0] mon_Q_A,mon_Q_B,mon_Q_C;
    repeat (iteration) @ (posedge CLK) begin
        mon_Q_A <= Q_A;
        mon_LOAD_A<= LOAD_A;
        mon_RCO_A <= RCO_A;

        mon_Q_B <= Q_B;
        mon_LOAD_B<= LOAD_B;
        mon_RCO_B<= RCO_B;

        mon_Q_C <= Q_C;
        mon_LOAD_C<= LOAD_C;
        mon_RCO_C <= RCO_C;
    end
 
endtask
