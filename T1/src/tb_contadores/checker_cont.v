task checker;

  input integer iteration;

  repeat (iteration) @ (posedge CLK) begin
  

      if(Q_A==Q_s)begin
      end
      else begin
        $fdisplay(log, "In Counter A: Time=%.0f Error dut: Q=%b scoreboard: Q=%b,", $time, Q_A,  Q_s);
      end

      if(RCO_A==RCO_s)begin
      end
      else begin
        $fdisplay(log, "In Counter A:  Time=%.0f Error dut: RCO=%b scoreboard: RCO=%b,", $time, RCO_A,  RCO_s);
      end
      if(LOAD_A==LOAD_s)begin
      end
      else begin
        $fdisplay(log, "In Counter A:  Time=%.0f Error dut: LOAD=%b scoreboard: LOAD=%b,", $time, LOAD_A,  LOAD_s);
      end


      if(Q_B==Q_s)begin
      end
      else begin
        $fdisplay(log, "In Counter B: Time=%.0f Error dut: Q=%b scoreboard: Q=%b,", $time, Q_B,  Q_s);
      end

      if(RCO_B==RCO_s)begin
      end
      else begin
        $fdisplay(log, "In Counter B:  Time=%.0f Error dut: RCO=%b scoreboard: RCO=%b,", $time, RCO_B,  RCO_s);
      end
      if(LOAD_B==LOAD_s)begin
      end
      else begin
        $fdisplay(log, "In Counter B:  Time=%.0f Error dut: LOAD=%b scoreboard: LOAD=%b,", $time, LOAD_B,  LOAD_s);
      end


      if(Q_C==Q_s)begin
      end
      else begin
        $fdisplay(log, "In Counter C: Time=%.0f Error dut: Q=%b scoreboard: Q=%b,", $time, Q_C,  Q_s);
      end

      if(RCO_C==RCO_s)begin
      end
      else begin
        $fdisplay(log, "In Counter C:  Time=%.0f Error dut: RCO=%b scoreboard: RCO=%b,", $time, RCO_C,  RCO_s);
      end
      if(LOAD_C==LOAD_s)begin
      end
      else begin
        $fdisplay(log, "In Counter C:  Time=%.0f Error dut: LOAD=%b scoreboard: LOAD=%b,", $time, LOAD_C,  LOAD_s);
      end
  
    
  end
endtask
