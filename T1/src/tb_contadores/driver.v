//Init task for clean output signals
task drv_init;

  begin
    ENABLE=1;
    D=0;
    MODO=0;
    @(negedge CLK)
      RESET = 1;
    @(negedge CLK)
      RESET = 1;
    @(negedge CLK)
      RESET = 1;
    @(negedge CLK)
      RESET = 0;
    @(negedge CLK)
      RESET = 0;
  end
endtask

//Drive request to the arbiter
task drv_prueba;
    reg [3:0] contador;

input integer iteration;
  repeat (iteration/4) begin  
    
    @(negedge CLK) begin
        if(RESET)begin
          contador<=0;
        end
        else begin
          contador<=contador+1;
          if(contador==0) begin
            MODO=MODO+1;
          end
          ENABLE = 1'b1;
          D=$random;
        end
        

    end
  end


  repeat (iteration/4) begin  
    
    @(negedge CLK) begin
        if(RESET)begin
          contador<=0;
        end
        else begin
          contador<=contador+1;
          if(contador==0) begin
            MODO=MODO+1;
          end
          ENABLE = 1'b0;
          D=$random;
        end
        

    end
  end

  repeat (iteration/4) begin  
    
    @(negedge CLK) begin
        if(RESET)begin
          contador<=0;
        end
        else begin
          contador<=contador+1;
          if(contador==0) begin
            MODO=MODO+1;
          end
          ENABLE = 1'b1;
          D=$random;
        end
        

    end
  end


  repeat (iteration/4) begin  
    
    @(negedge CLK) begin
        if(RESET)begin
          contador<=0;
        end
        else begin
          contador<=contador+1;
          if(contador[2:0]==0) begin
            MODO=$random;
          end
          ENABLE = 1'b1;
          D=$random;
        end
        

    end
  end


  
endtask


