`include "clock.v"
`include "contadorA.v"
`include "contadorB.v"
`include "contadorC.v"
`include "count_tb.v"

module tb_top;



    wire [3:0] D, Q_A,Q_B,Q_C;
    wire [1:0] MODO;

    clk clock (.clock (CLK));

    count_tb  count_tb(
    .CLK (CLK),
    .ENABLE (ENABLE),
    .RESET (RESET),
    .D (D),
    .MODO (MODO),
    .Q_A (Q_A),
    .RCO_A (RCO_A),
    .LOAD_A (LOAD_A),
    .Q_B (Q_B),
    .RCO_B (RCO_B),
    .LOAD_B (LOAD_B),
    .Q_C (Q_C),
    .RCO_C (RCO_C),
    .LOAD_C (LOAD_C)
    );

    counterA A(
    .clk (CLK),
    .enable (ENABLE),
    .reset (RESET),
    .D (D),
    .mode (MODO),
    .Q (Q_A),
    .rco (RCO_A),
    .load (LOAD_A)
    );

     counterB B(
    .clk (CLK),
    .enable (ENABLE),
    .reset (RESET),
    .D (D),
    .mode (MODO),
    .Q (Q_B),
    .rco (RCO_B),
    .load (LOAD_B)
    );

     counterC C(
    .clk (CLK),
    .enable (ENABLE),
    .reset (RESET),
    .D (D),
    .mode (MODO),
    .Q (Q_C),
    .rco (RCO_C),
    .load (LOAD_C)
    );

endmodule




