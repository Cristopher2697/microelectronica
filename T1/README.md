### Descripcion del proyecto

-  Se realiza la verificación funcional de tres módulos contadores para garantizar el correcto funcionamiento de los diseños.1. 
- Se realiza una descripcion detallada del proyecto en el documento llamado ReporteT1 ubicado en el directorio Reporte.

------------

### Makefile

- El makefile posee dos objetivos :

  1. contadores: Ejecuta todas las pruebas implementadas y muestra las graficas en gtkwave usando el archivo previamente creado de gtkw.Es el objetivo por defecto.
  2. clean: Elimina la carpeta tmp que contiene todos los archivos creados por la simulacion: log, .vcd, .obj
  

- Para ejecutar las pruebas basta con abrir una terminal dentro del directorio src y ejecutar:
`make` 
- Tambien puede probar
`make contadores`
`make clean` 
- El makefile se encuentra en el directorio src/

------------
### Dependencias

- Debe tener icarus verilog 
`sudo apt-get install iverilog`
- Debe tener gtkwave
`sudo apt-get install gtkwave`






